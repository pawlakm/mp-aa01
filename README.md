# MP-AA01 Audio Probe

The objective of this project is to provide a LM386-based mono audio amplifier PCB that fits in a 1590A Hammond enclosure.
The resulting PCB has been designed so it can be used together with a 3.2cm speaker and dual banana jack to build a [tiny all-in-one amplified audio probe](https://gitlab.com/pawlakm/mp-aa01/-/raw/master/Pictures/MP-AA01-finished_plugged.jpg).

<div align="center">
![1590A all-in-one amplified audio probe based on the MP-AA01 PCB](https://gitlab.com/pawlakm/mp-aa01/-/wikis/images/MP-AA01-finished_plugged.png)
![MP-AA01 based audio probe open enclosure](https://gitlab.com/pawlakm/mp-aa01/-/wikis/images/MP-AA01-finished_open.png)
</div>

[Schematics](https://gitlab.com/pawlakm/mp-aa01/-/wikis/documentation/Schematics) and PCB layout are provided as KiCAD project files. Gerber and drill files needed to produce your own PCB are available in the [releases](https://gitlab.com/pawlakm/mp-aa01/-/releases) section of this repository. The [BOM](https://gitlab.com/pawlakm/mp-aa01/-/wikis/documentation/BOM) and [build steps](https://gitlab.com/pawlakm/mp-aa01/-/wikis/documentation/Build-Steps) for this project are available as wiki pages.

You can buy a ready to solder PCB to support me and help me share other projects with you. In order to buy a MP-AA01 PCB, you can contact me on [DIYstompboxes forum](https://www.diystompboxes.com/smfforum/) (nickname kraal) or [create an issue](https://gitlab.com/pawlakm/mp-aa01/-/issues) here on GitLab (use the PCB order issue type). 

If you don't need a PCB but still want to support this project, you can [make a donation using PayPal](https://www.paypal.com/cgi-bin/webscr?cmd=_donations&business=CRWLBWFHVU968&item_name=Support+for+the+MP-AA01+project&currency_code=CHF). Thank you in advance, your help is greatly appreciated!

[![test](https://www.paypalobjects.com/en_US/CH/i/btn/btn_donateCC_LG.gif)](https://www.paypal.com/cgi-bin/webscr?cmd=_donations&business=CRWLBWFHVU968&item_name=Support+for+the+MP-AA01+project&currency_code=CHF)

[MP-AA01 Audio Amplifier (1590A PCB)](https://gitlab.com/pawlakm/mp-aa01) by <span property="cc:attributionName">Michel Pawlak</span> is licensed under <a rel="license" href="https://creativecommons.org/licenses/by-nc-nd/4.0">CC BY-NC-ND 4.0 <img height="22px" src="https://mirrors.creativecommons.org/presskit/icons/cc.svg?ref=chooser-v1" /><img height="22px" src="https://mirrors.creativecommons.org/presskit/icons/by.svg?ref=chooser-v1" /><img height="22px" src="https://mirrors.creativecommons.org/presskit/icons/nc.svg?ref=chooser-v1" /><img height="22px" src="https://mirrors.creativecommons.org/presskit/icons/nd.svg?ref=chooser-v1" /></a>
